package TheMummy
class ScoreBoard(scores: List[Score]) {

  def readScores(): Array[Score] = {
    val cscores = this.scores
    cscores.toArray
  }

  def readScoreLst(): List[Score] = {
    val cscores = this.scores
    cscores
  }

  def rankScores(): ScoreBoard = {
    val a = this.readScores
    for (j <- 0 until a.length - 1) {
      for (i <- 0 until a.length - 1 - j) {
        if (a(i).readScore > a(i + 1).readScore) {
          val tmp = a(i)
          a(i) = a(i + 1)
          a(i + 1) = tmp
        }
      }
    }
    val b = a.toList
    new ScoreBoard(b.reverse)
  }

  def addScore(s: Score): ScoreBoard = {
    new ScoreBoard(s :: this.scores)
  }

  def scoreConvert(s: Score): xml.Node = {
    <highScore score={s.readScore().toString} name={s.readName()} />
  }

  def exportScores(ls: List[Score]): xml.Node = {
    <scores>
      {ls.map(scoreConvert(_))}
    </scores>
  }

}
