![](images/TheMummy.png?raw=true)
# the-mummy
A silly arcade game in ScalaFX. Not sure why Nicholas Cage is here, or why he is carrying a wobbly-stack of yellow balls, but whatever.

Dependencies:
- Scala 2.12 +
- Java 8 +

Run with the helper script `start.sh` if you do not have ScalaFX in your classpath.

How to play:
Click "Start" and use the left and right arrow keys to move. Hitting the "RageCage" button increases the difficulty by spawning more tomato-asteroids. Clicking Highscores will view the Highscores file as of the execution of the program (to see your highscore added to the list restart the game and re-open highscores).

- Turner Hall 2017
