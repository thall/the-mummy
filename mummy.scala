/*
 The Mummy!

 DISCLAIMER:
 For some reason when I was making this game I incorrectly recalled
 Nicholas Cage as being the star of the movie known as the Mummy
 when it was in fact Brendan Fraser. I have no idea how I mixed
 that up. Maybe National Treasure reminded me of The Mummy?

 Anyways, just enjoy dodging tomato-colored asteroids hurled
 at you by Aharon Ipalé while holding a wobbly stack of
 grapefruits which hold the key to not having time itself
 freeze.

 Turner Hall 2017 - Do as you like, but only with Emacs
 */

package TheMummy

object Mummy {
  def main(args: Array[String]): Unit = {
    import scalafx.Includes._
    import scalafx.application.JFXApp
    import scalafx.scene.Scene
    import scalafx.scene.control._
    import scalafx.scene.shape._
    import scalafx.event.ActionEvent
    import scalafx.scene.input._
    import scalafx.scene.layout._
    import scalafx.animation.AnimationTimer
    import scalafx.scene.image._
    import scalafx.scene.text.Font
    import scalafx.scene.paint.Color
    import xml._
    import sys.process._

    val username = "whoami".!!.trim // get the system username, for leaderboards

    val highscores = XML.loadFile("highscores.xml")
    val scores = highscores \\ "highScore" // NodeSeq of all scores
    val scoreParse: List[Score] = ((scores
      .map(_.\\("@name").toString))
      .zip(scores.map(_.\\("@score").toString.toInt)))
      .toList
      .map(x => new Score(x._2, x._1))
    val scoreList = new ScoreBoard(scoreParse)
    // above parses the xml file and makes a List of Score objects with every score and the associated name
    val scoreNums = highscores \\ "@score"
    // make an array of score ints to check if current score is a highscore later
    val highArr: Array[Int] = scoreNums.toArray.map(_.text.toInt)

    val app = new JFXApp {
      stage = new JFXApp.PrimaryStage {
        title = "The Mummy"
        scene = new Scene(1400, 1000) {
          val borderFrame = new BorderPane
          val mainView = new Pane
          mainView.prefHeight = 900
          val menuBar = new HBox
          val startButton = new Button("Start")
          val scoreButton = new Button("High Scores")
          val rageButton = new Button("RAGECAGE"); var rageMult = (1)
          menuBar.children = List(startButton, scoreButton, rageButton)
          // stuff for main panel (game window)
          val bkg =
            new Image( 
              "file:bkg.png")
          val background = new ImageView(bkg)
          background.layoutX = -700
          background.layoutY = -200
          val face =
            new Image( 
              "file:thecage.png") 
          val view = new ImageView(face)
          val cage = Circle(20, 220, 1)
          val rBorder = Rectangle(900, 0, 800, 1200)
          val lBorder = Rectangle(0, 0, 200, 1200)
          val underBorder = Rectangle(0, 910, 2000, 6000)
          val gameDone = new Font(40)
          cage.centerX = 500
          cage.centerY = 740
          view.resize(30, 30)
          // need some starting position for cage
          var asteroids = List[Circle]()
          var cageTail = List[Circle]()
          val tailLength = 12
          var tailLocs = List.fill(tailLength)(cage.centerX.value) // first value is equal to current cage location
          val scoreView = Label(highArr.max.toString)
          scoreView.font = gameDone
          scoreView.layoutX = 920
          scoreView.layoutY = 200
          mainView.children = List(background,
                                   cage,
                                   view,
                                   rBorder,
                                   lBorder,
                                   underBorder,
                                   scoreView)
          borderFrame.center = mainView
          borderFrame.bottom = menuBar
          root = borderFrame
          // various variables for the game
          var leftKey = false
          var rightKey = false
          var lastTime = 0L
          var lastTailTime = 0
          var score = 0
          var scoreString: String = highArr.max.toString
          var aColor = Color.Brown
          val r = scala.util.Random

          // create cageTail
          for (i <- 0 until tailLength) {
            val tSegment =
              if (i != 0)
                Circle(cage.centerX.value, cage.centerY.value + i * 15, 10)
              else Circle(cage.centerX.value, cage.centerY.value + 15, 10)
            tSegment.fill = Color.Yellow
            cageTail ::= tSegment
            content += tSegment
          }

          onKeyPressed = (e: KeyEvent) => { // edits vars used in animationtimer
            if (e.code == KeyCode.Left) leftKey = true
            if (e.code == KeyCode.Right) rightKey = true
          }

          onKeyReleased =
            (e: KeyEvent) => { // edits vars used in animationtimer
              if (e.code == KeyCode.Left) leftKey = false
              if (e.code == KeyCode.Right) rightKey = false
            }

          rageButton.onAction = (b: ActionEvent) => {
            rageMult = 2
            aColor = Color.Red
          }

          startButton.onAction = (b: ActionEvent) => {
            timer.start
            startButton.disable = true
          }

          /*
           Window displaying highscores, stored in XML.
           Scoreboard names are obtained with "whoami".
           Besides, Windows users don't deserve recognition anyways.
           */
          scoreButton.onAction = (b: ActionEvent) => {
            timer.stop // no use in allowing the game to keep running
            stage = new JFXApp.PrimaryStage {
              title = "Mummy Scores:"
              scene = new Scene(400, 900) {
                // replace scoreList with a sorted version
                val sortedScores = scoreList.rankScores().readScoreLst()
                val boardNames =
                  sortedScores
                    .map(_.readName)
                    .zip(sortedScores.map(_.readScore))
                val scoreBoard = new ListView(boardNames)
                content = scoreBoard
              }
            }
          }
          val timer: AnimationTimer = AnimationTimer(t => {

            if (lastTailTime == 3) {
              // Add a new tail location to the list and drop the oldest one
              val currentCage = cage.centerX.value
              tailLocs = (currentCage :: tailLocs).dropRight(1)
              lastTailTime = 0
              for (i <- 0 until 12) {
                cageTail(i).centerX = tailLocs(i)
              }

            }

            val oobLeft =
              Shape.intersect(cage, lBorder).boundsInLocal.value.isEmpty
            val oobRight =
              Shape.intersect(cage, rBorder).boundsInLocal.value.isEmpty
            // might be a bit counter-intuitive, checking if NOT oob
            if (leftKey == true && oobLeft == true) {
              cage.centerX = cage.centerX.value - 4
            }

            if (rightKey == true && oobRight == true) {
              cage.centerX = cage.centerX.value + 4
            }

            lastTime = lastTime + 1
            // This is where the code for "RAGECAGE" is
            if (lastTime > 20 / rageMult) {
              val a = Circle(r.nextInt(700) + 200, -30, 20, 20)
              a.fill = aColor
              asteroids ::= a
              content += a
              lastTime = 0L
            }
            // asteroids move down
            for (k <- asteroids) {
              k.centerY = k.centerY.value + 4 // movement rate
            }
            // checks to see if the yellow balls touch the falling ones
            val alive = asteroids.forall(a => {
              cageTail.forall(b => {
                Shape.intersect(a, b).boundsInLocal.value.isEmpty
              })
            })
            // As you guess when alive does not equal true...
            // Nicholas cage is frozen in time forever
            if (alive != true) {
              val gameOver = new Label("Game Over")
              gameOver.font = gameDone
              gameOver.layoutX = 600
              gameOver.layoutY = 500
              content += gameOver
              val highestScore =
                if (highArr.max.toInt > score) highArr.max else score + 1
              val highLabel =
                new Label("High Score: " + highestScore.toString)
              highLabel.font = gameDone
              highLabel.layoutX = 600
              highLabel.layoutY = 600
              content += highLabel
              val newscore = new Score(score + 1, username)
              val newScores = scoreList.addScore(newscore)
              xml.XML.save("highscores.xml",
                           newScores.exportScores(newScores.readScoreLst()))
              startButton.disable = true
              timer.stop

            }
            view.relocate(cage.centerX.value, cage.centerY.value + 100)

            lastTailTime = lastTailTime + 1
            score += 1 //score is basically cycles alive
            scoreString = score.toString
            // update score graphic realtime
            scoreView.text = scoreString

          })
        }
      }
    }

    app.main(args)
  }
}
