package TheMummy
class Score(score: Int, name: String) {
  def readScore(): Int = {
    val cscore = this.score
    cscore
  }

  def readName(): String = {
    val cname = this.name
    cname
  }
}
